## Synopsis

This is the Pre-compiled version of the Notes De-Identification Software used to de-identify patient Notes from one database column to another


## Installation

Provide code examples and explanations of how to get the project.

## API Reference

Depending on the size of the project, if it is small and simple enough the reference docs can be added to the README. For medium size to larger projects it is important to at least provide a link to where the API reference docs live.


## Contributors

Based on the code and Contributors found here : https://bitbucket.org/MCW_BMI/notes-deidentification

## License

MIT
