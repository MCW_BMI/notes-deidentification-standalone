#!/bin/bash
#
# Script to call the De-Identification Process 
# George Kowalski 
#
java -Xms2048M -Xmx4096M -jar lib/DeIdentificationX2-0.0.1-SNAPSHOT.one-jar.jar \
    -login "userid" \
    -password "password" \
    -dburl "jdbc:postgresql://server:5432/database" \
    -dbdriver "org.postgresql.Driver" \
    -nthreads 5 \
    -recordsperthread 100 \
    -query "select NOTE_ID as id, NOTE_ID as note_id, NOTE_TEXT as note_text, DATE_OFF as date_off, first_name || ' ' || last_name as PATIENT_NAME from GPC_NOTE_TEXT order by id" \
    -deidnotestablename "DEID_NOTES" \
    -whitelistfilename "whitelist.txt" \
    -blacklistfilename "blacklist.txt" \
    -namedentityrecognitionclass "deidentification.mcw.NamedEntityRecognitionMCWx3" \
    -regexdeidentificationclass "deidentification.mcw.DeidentificationRegexMCW" \
